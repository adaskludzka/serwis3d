package pl.teb.spring.domain.service;

import org.junit.jupiter.api.Test;
import pl.teb.spring.domain.store.UserStore;
import pl.teb.spring.infrastructure.entity.User;

import static org.junit.jupiter.api.Assertions.*;

class UserServiceTest {
    private final String FIRSTNAME = "FIRSTNAME";
    private final String LASTNAME = "LASTNAME";
    private final String EMAIL = "EMAIL@OP.PL";
    private final String PASSWORD = "9090Hs";
    private final UserStore userStore = new UserRepositoryMock();
    private final UserService userService = new UserService(userStore);

    @Test
    public void userShouldBeNotNull(){
        User user = userService.createUser(FIRSTNAME, LASTNAME, EMAIL, PASSWORD);
        assertNotNull(user);
    }

    @Test
    public void userDataShouldBeEquals(){
        User user = userService.createUser(FIRSTNAME, LASTNAME, EMAIL, PASSWORD);
        assertEquals(user.getFirstname(),FIRSTNAME);
        assertEquals(user.getLastname(),LASTNAME);
        assertEquals(user.getEmail(),EMAIL);
        assertEquals(user.getPassword(),PASSWORD);
    }

    @Test
    public void userPasswordShouldBeUpdated(){
        User user = userService.createUser(FIRSTNAME, LASTNAME, EMAIL, PASSWORD);
        String newPassword = "45hhJ";
        User updatedUser = userService.updatePassword(user.getUuid(),newPassword);
        assertEquals(user.getFirstname(),updatedUser.getFirstname());
        assertEquals(user.getLastname(),updatedUser.getLastname());
        assertEquals(user.getEmail(),updatedUser.getEmail());
        assertEquals(newPassword,updatedUser.getPassword());
    }

}