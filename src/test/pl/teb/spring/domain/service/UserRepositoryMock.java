package pl.teb.spring.domain.service;

import pl.teb.spring.domain.store.UserStore;
import pl.teb.spring.infrastructure.entity.User;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

public class UserRepositoryMock implements UserStore {
    private final List<User> users = new ArrayList<>();
    private Long id = 1L;
    @Override
    public User save(User user) {
        User toList = new User(id, UUID.randomUUID().toString(), user.getFirstname(), user.getLastname(),
                user.getEmail(), user.getPassword());
        id++;
        users.add(toList);
         return toList;
    }

    @Override
    public Optional<User> findByUuid(String uuid) {
        return users.stream()
                .filter(user -> user.getUuid().equals(uuid))
                .findAny();

    }
}
