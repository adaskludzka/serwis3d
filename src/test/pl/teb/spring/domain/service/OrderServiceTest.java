package pl.teb.spring.domain.service;

import org.junit.jupiter.api.Test;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.thymeleaf.TemplateEngine;
import pl.teb.spring.domain.store.OrderStore;
import pl.teb.spring.domain.store.UserStore;
import pl.teb.spring.infrastructure.entity.Order;
import pl.teb.spring.infrastructure.entity.Status;
import pl.teb.spring.infrastructure.entity.User;

import javax.xml.bind.annotation.XmlType;

import java.util.Random;

import static org.junit.jupiter.api.Assertions.*;

public class OrderServiceTest {
    private final String PRINTER_MODEL = "PRINTERMODEL";
    private final String DESCRIPTION = "DESCRIPTION";
    private final String FIRSTNAME = "FIRSTNAME";
    private final String LASTNAME = "LASTNAME";
    private final String EMAIL = "EMAIL@OP.PL";
    private final String PASSWORD = "9090Hs";
    private final Status DEFAULT_STATUS = Status.RECIVED;
    private final UserStore userStore = new UserRepositoryMock();
    private final UserService userService = new UserService(userStore);
    private final OrderStore orderStore = new OrderRepositoryMock();
    private final MailService mailService = new MailService(new JavaMailSenderImpl(), new TemplateEngine());
    private final OrderService orderService = new OrderService(orderStore, mailService);


    @Test
    public void orderShouldNotBeNull(){
        User user = userService.createUser(FIRSTNAME, LASTNAME, EMAIL, PASSWORD);
        Order order = orderService.receiveOrder(user, PRINTER_MODEL, DESCRIPTION);
        assertNotNull(order);
    }

    @Test
    public void orderDataShouldBeEquals(){
        User user = userService.createUser(FIRSTNAME, LASTNAME, EMAIL, PASSWORD);
        Order order = orderService.receiveOrder(user, PRINTER_MODEL, DESCRIPTION);
        assertEquals(order.getPrinterModel(), PRINTER_MODEL);
        assertEquals(order.getDescription(), DESCRIPTION);
        assertEquals(order.getUser(), user);
        assertEquals(order.getStatus(), DEFAULT_STATUS);

    }

    @Test
    public void orderStatusShouldBeUpdated(){
        User user = userService.createUser(FIRSTNAME, LASTNAME, EMAIL, PASSWORD);
        Order order = orderService.receiveOrder(user, PRINTER_MODEL, DESCRIPTION);
        Order updatedOrder = orderService.updateStatus(order.getUuid(), generateStatus());
        assertEquals(order.getPrinterModel(), updatedOrder.getPrinterModel());
        assertEquals(order.getDescription(), updatedOrder.getDescription());
        assertEquals(order.getUser(), updatedOrder.getUser());
        assertNotEquals(order.getStatus(), updatedOrder.getStatus());
    }

    private Status generateStatus(){
        Random random = new Random();
        boolean value = random.nextBoolean();
        if(value){
            return Status.IN_PROGRES;
        }
        else{
            return Status.DONE;
        }
    }

}
