package pl.teb.spring.domain.service;

import pl.teb.spring.domain.store.OrderStore;
import pl.teb.spring.infrastructure.entity.Order;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

public class OrderRepositoryMock implements OrderStore {
    private final List<Order> orders = new ArrayList<>();
    private Long id = 1L;
    @Override
    public Order save(Order order) {
        Order orderToList = new Order(id, UUID.randomUUID().toString(), order.getUser(), order.getPrinterModel(),
                order.getDescription(), order.getStatus());
        id++;
        orders.add(orderToList);
        return orderToList;
    }

    @Override
    public Optional<Order> findByUuid(String uuid) {
        return orders.stream()
                .filter(order -> order.getUuid().equals(uuid))
                .findAny();
    }
}
