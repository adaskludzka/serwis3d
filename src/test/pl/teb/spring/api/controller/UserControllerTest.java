package pl.teb.spring.api.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import pl.teb.spring.api.dto.request.UserRequest;
import pl.teb.spring.domain.service.UserRepositoryMock;
import pl.teb.spring.domain.service.UserService;
import pl.teb.spring.domain.store.UserStore;

import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

class UserControllerTest {
    private final String FIRSTNAME = "FIRSTNAME";
    private final String LASTNAME = "LASTNAME";
    private final String EMAIL = "EMAIL@OP.PL";
    private final String PASSWORD = "9090Hs";
    private final String CREATE_USER_ENDPOINT = "/user";
    @Autowired
    private MockMvc mvc;
    @Autowired
    private ObjectMapper mapper;
    @Autowired
    private UserStore userStore;
    @Autowired
    private UserService userService;
    @Autowired
    private UserController userController;

    @Test
    public void createUserShouldReturnStatus200() throws Exception {
        UserRequest userRequest = UserRequest.builder()
                .firstname(FIRSTNAME)
                .lastname(LASTNAME)
                .email(EMAIL)
                .password(PASSWORD)
                .build();

        createUserRequest(userRequest)
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON));
    }

    private ResultActions createUserRequest(UserRequest request) throws Exception {
        return mvc.perform(MockMvcRequestBuilders.post(CREATE_USER_ENDPOINT)
                .content(mapper.writeValueAsString(request)).contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON));
    }

}