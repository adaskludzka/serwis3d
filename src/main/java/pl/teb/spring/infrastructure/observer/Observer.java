package pl.teb.spring.infrastructure.observer;

public interface Observer {
    void update(Observable o);

}
