package pl.teb.spring.infrastructure.observer;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;
import pl.teb.spring.domain.service.MailService;
import pl.teb.spring.infrastructure.entity.Order;

@Component
@AllArgsConstructor
public class MailObserver {
    private final MailService mailService;


}
