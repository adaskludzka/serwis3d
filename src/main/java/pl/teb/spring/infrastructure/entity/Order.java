package pl.teb.spring.infrastructure.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import pl.teb.spring.infrastructure.observer.Observable;
import pl.teb.spring.infrastructure.observer.Observer;

import javax.persistence.*;

import static javax.persistence.GenerationType.SEQUENCE;

@Entity
@Table(name = "app_order")
@AllArgsConstructor
@NoArgsConstructor
@Getter
public class Order implements Observable {
    @Id
    @SequenceGenerator(
            name = "order_id_sequence",
            sequenceName = "order_id_sequence",
            allocationSize = 1
    )
    @GeneratedValue(
            strategy = SEQUENCE,
            generator = "order_id_sequence"
    )
    private Long id;
    private String uuid;
    @ManyToOne
    @JoinColumn(name = "user_id")
    private User user;
    private String printerModel;
    private String description;
    private Status status;

    public Order(String uuid, User user, String printerModel, String description) {
        this.uuid = uuid;
        this.user = user;
        this.printerModel = printerModel;
        this.description = description;
        this.status = Status.RECIVED;
    }

    @Override
    public void addObserver(Observer o) {

    }

    @Override
    public void removeObserver(Observer o) {

    }

    @Override
    public void notifyObservers() {

    }
}
