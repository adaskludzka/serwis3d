package pl.teb.spring.infrastructure.entity;

public enum Status {
    RECIVED,
    IN_PROGRES,
    DONE
}
