package pl.teb.spring.infrastructure.converter;

import org.springframework.core.convert.converter.Converter;
import pl.teb.spring.api.dto.response.UserResponse;
import pl.teb.spring.infrastructure.entity.User;

public class UserResponseToUser implements Converter<UserResponse, User> {

    @Override
    public User convert(UserResponse userResponse) {
        return new User(userResponse.getUuid(), userResponse.getFirstname(), userResponse.getLastname(),
                userResponse.getEmail(), userResponse.getPassword());
    }
}
