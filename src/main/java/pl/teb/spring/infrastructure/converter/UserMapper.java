//package pl.teb.spring.infrastructure.converter;
//
//import org.mapstruct.Mapper;
//import org.mapstruct.Mapping;
//import org.mapstruct.MappingConstants;
//import org.mapstruct.ReportingPolicy;
//import org.mapstruct.factory.Mappers;
//import pl.teb.spring.api.dto.response.UserResponse;
//import pl.teb.spring.infrastructure.entity.User;
//
//@Mapper(
//        componentModel = MappingConstants.ComponentModel.SPRING,
//        unmappedTargetPolicy = ReportingPolicy.ERROR)
//public interface UserMapper {
//    UserMapper INSTANCE = Mappers.getMapper(UserMapper.class);
//
//    UserResponse toUserResponse(User user);
//
//    @Mapping(target = "id", ignore = true)
//    User toUser(UserResponse userResponse);
//}
