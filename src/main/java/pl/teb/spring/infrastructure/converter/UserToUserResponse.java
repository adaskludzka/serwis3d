package pl.teb.spring.infrastructure.converter;

import org.springframework.core.convert.converter.Converter;
import pl.teb.spring.api.dto.response.UserResponse;
import pl.teb.spring.infrastructure.entity.User;

public class UserToUserResponse implements Converter<User, UserResponse> {

    @Override
    public UserResponse convert(User user) {
        return new UserResponse(
                user.getUuid(),
                user.getFirstname(),
                user.getLastname(),
                user.getEmail(),
                user.getPassword());
    }
}
