package pl.teb.spring.infrastructure.converter;

import org.springframework.core.convert.converter.Converter;
import pl.teb.spring.api.dto.response.OrderResponse;
import pl.teb.spring.infrastructure.entity.Order;

public class OrderToOrderResponse implements Converter<Order, OrderResponse> {

    @Override
    public OrderResponse convert(Order order) {
        return OrderResponse.builder()
                .uuid(order.getUuid())
                .printerModel(order.getPrinterModel())
                .description(order.getDescription())
                .user(order.getUser())
                .status(order.getStatus())
                .build();
    }
}
