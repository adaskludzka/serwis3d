package pl.teb.spring.infrastructure.adapter;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;
import pl.teb.spring.domain.store.OrderStore;
import pl.teb.spring.infrastructure.entity.Order;
import pl.teb.spring.infrastructure.repository.OrderRepository;
import pl.teb.spring.infrastructure.repository.UserRepository;

import java.util.Optional;

@AllArgsConstructor
@Component
public class OrderAdapter implements OrderStore {
    private final OrderRepository orderRepository;
    private final UserRepository userRepository;

    @Override
    public Order save(Order order) {
        userRepository.save(order.getUser());
        return orderRepository.save(order);
    }

    @Override
    public Optional<Order> findByUuid(String uuid) {
        return orderRepository.findByUuid(uuid);
    }
}
