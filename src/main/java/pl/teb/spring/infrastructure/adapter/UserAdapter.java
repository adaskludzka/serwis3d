package pl.teb.spring.infrastructure.adapter;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;
import pl.teb.spring.domain.store.UserStore;
import pl.teb.spring.infrastructure.entity.User;
import pl.teb.spring.infrastructure.repository.UserRepository;

import java.util.Optional;

@AllArgsConstructor
@Component
public class UserAdapter implements UserStore {
    private final UserRepository userRepository;

    @Override
    public User save(User user) {
        return userRepository.save(user);
    }

    @Override
    public Optional<User> findByUuid(String uuid) {
        return userRepository.findByUuid(uuid);
    }
}
