package pl.teb.spring.infrastructure.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Lazy;
import org.springframework.core.convert.ConversionService;
import org.springframework.format.FormatterRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import pl.teb.spring.infrastructure.converter.OrderToOrderResponse;
import pl.teb.spring.infrastructure.converter.UserResponseToUser;
import pl.teb.spring.infrastructure.converter.UserToUserResponse;

@Configuration
public class SpringConvertersConfig implements WebMvcConfigurer {

    @Override
    public void addFormatters(FormatterRegistry registry) {
        registry.addConverter(new UserToUserResponse());
        registry.addConverter(new OrderToOrderResponse());
        registry.addConverter(new UserResponseToUser());
    }
}
