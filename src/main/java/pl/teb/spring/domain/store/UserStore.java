package pl.teb.spring.domain.store;

import pl.teb.spring.infrastructure.entity.User;

import java.util.Optional;

public interface UserStore {
    User save(User user);
    Optional<User> findByUuid(String uuid);
}
