package pl.teb.spring.domain.store;

import pl.teb.spring.infrastructure.entity.Order;

import java.util.Optional;

public interface OrderStore {
    Order save(Order order);
    Optional<Order> findByUuid(String uuid);
}
