package pl.teb.spring.domain.service;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import pl.teb.spring.domain.exeption.AppExceptionCode;
import pl.teb.spring.domain.store.UserStore;
import pl.teb.spring.infrastructure.entity.User;

import java.util.UUID;

@Service
@AllArgsConstructor
public class UserService {

    private final UserStore userStore;

    public User createUser(String firstname, String lastname, String email, String password) {
        return userStore.save(new User(UUID.randomUUID().toString(), firstname, lastname, email, password));
    }

    public User updatePassword(String uuid, String newPassword) {
        var user = userStore.findByUuid(uuid).orElseThrow(AppExceptionCode.NO_SUCH_USER::createException);
        User updated = new User(user.getId(), user.getUuid(), user.getFirstname(), user.getLastname(), user.getEmail(),
                newPassword);
        return userStore.save(updated);
    }

}
