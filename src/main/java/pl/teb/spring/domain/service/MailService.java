package pl.teb.spring.domain.service;


import lombok.AllArgsConstructor;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;
import org.thymeleaf.context.IContext;
import pl.teb.spring.infrastructure.entity.Order;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

@Service
@AllArgsConstructor
public class MailService {
    private final JavaMailSender javaMailSender;
    private final TemplateEngine templateEngine;

    public void sendMail(Order order) {
        Context context = new Context();
        MimeMessage message = javaMailSender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(message);
        try{
            helper.setTo(order.getUser().getEmail());
            helper.setSubject("Przyjęto zamówienie.");
            String html = templateEngine.process("RecivedOrderTemplate.html", context);
            helper.setText(html);
        }catch(MessagingException e)
        { e.printStackTrace();}
        javaMailSender.send(message);
    }
}
