package pl.teb.spring.domain.service;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import pl.teb.spring.domain.exeption.AppExceptionCode;
import pl.teb.spring.domain.store.OrderStore;
import pl.teb.spring.infrastructure.entity.Order;
import pl.teb.spring.infrastructure.entity.Status;
import pl.teb.spring.infrastructure.entity.User;
import pl.teb.spring.infrastructure.observer.Observable;
import pl.teb.spring.infrastructure.observer.Observer;

import java.util.UUID;

@Service
public class OrderService implements Observer {
    private final OrderStore orderStore;
    private final MailService mailService;

    public OrderService(OrderStore orderStore, MailService mailService) {
        this.orderStore = orderStore;
        this.mailService = mailService;
    }

    public Order receiveOrder(User user, String printerModel, String description){
        Order order = orderStore.save(new Order(UUID.randomUUID().toString(), user, printerModel, description));
        mailService.sendMail(order);
        return order;
    }

    public Order updateStatus(String uuid, Status status) {
        var order = orderStore.findByUuid(uuid).orElseThrow(AppExceptionCode.NO_SUCH_ORDER::createException);
        Order updated = new Order(order.getId(), order.getUuid(), order.getUser(), order.getPrinterModel(),
                order.getDescription(), status);
        update(updated);
        return orderStore.save(updated);
    }

    @Override
    public void update(Observable o) {
        if (o instanceof Order) {
            Order order = (Order) o;
            mailService.sendMail(order);
        }
    }

}
