package pl.teb.spring.api.dto.response;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import pl.teb.spring.infrastructure.entity.Status;
import pl.teb.spring.infrastructure.entity.User;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Getter
public class OrderResponse {

    private String uuid;
    private User user;
    private String printerModel;
    private String description;
    private Status status;
}
