package pl.teb.spring.api.dto.request;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Email;


@Getter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class UserRequest {

    private String firstname;
    private String lastname;
    //@Email(message = "Email is not valid.", regexp = "\\b[A-Z0-9._%-]+@[A-Z0-9.-]+\\.[A-Z]{2,4}\\b")
    private String email;
    private String password;
}
