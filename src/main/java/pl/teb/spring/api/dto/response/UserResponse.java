package pl.teb.spring.api.dto.response;

import lombok.*;


@NoArgsConstructor
@AllArgsConstructor
@Getter
@Builder
public class UserResponse {

    private String uuid;
    private String firstname;
    private String lastname;
    private String email;
    private String password;

}
