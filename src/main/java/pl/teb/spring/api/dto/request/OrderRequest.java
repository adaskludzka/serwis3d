package pl.teb.spring.api.dto.request;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import pl.teb.spring.api.dto.response.UserResponse;

import javax.validation.constraints.NotNull;

@AllArgsConstructor
@NoArgsConstructor
@Getter
public class OrderRequest {

    @NotNull
    private UserResponse userResponse;
    private String printerModel;
    private String description;
}
