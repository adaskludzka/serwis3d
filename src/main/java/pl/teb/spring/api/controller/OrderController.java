package pl.teb.spring.api.controller;

import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import lombok.AllArgsConstructor;
import org.springframework.core.convert.ConversionService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pl.teb.spring.api.dto.request.OrderRequest;
import pl.teb.spring.api.dto.response.OrderResponse;
import pl.teb.spring.api.exeption.AppExceptionHandler;
import pl.teb.spring.domain.service.OrderService;
//import pl.teb.spring.infrastructure.converter.UserMapper;
import pl.teb.spring.infrastructure.entity.Order;
import pl.teb.spring.infrastructure.entity.Status;
import pl.teb.spring.infrastructure.entity.User;

import javax.validation.Valid;
import java.util.Objects;

@RestController
@RequestMapping("/order")
public class OrderController {
    private final OrderService orderService;
    private final ConversionService conversionService;
    //private final UserMapper userMapper;


    public OrderController(OrderService orderService, ConversionService conversionService) {
        this.orderService = orderService;
        this.conversionService = conversionService;
    }

    @PostMapping
    @ApiResponses(
            value = {
                    @ApiResponse(responseCode = "200"),
                    @ApiResponse(responseCode = "400", content = @Content(schema = @Schema(implementation =
                            AppExceptionHandler.Error.class))),
                    @ApiResponse(responseCode = "500", content = @Content(schema = @Schema(implementation =
                            AppExceptionHandler.Error.class)))
            })
    public ResponseEntity<OrderResponse> receiveOrder(@Valid @RequestBody OrderRequest orderRequest) {
      //  User user = userMapper.toUser(orderRequest.getUserResponse());
       // return ResponseEntity.ok(Objects.requireNonNull(conversionService.convert(orderService.receiveOrder(user,
          //      orderRequest.getPrinterModel(), orderRequest.getDescription()), OrderResponse.class)));
        return null;
    }

    @PutMapping
    @ApiResponses(
            value = {
                    @ApiResponse(responseCode = "200"),
                    @ApiResponse(responseCode = "400", content = @Content(schema = @Schema(implementation =
                            AppExceptionHandler.Error.class))),
                    @ApiResponse(responseCode = "500", content = @Content(schema = @Schema(implementation =
                            AppExceptionHandler.Error.class)))
            })
    public ResponseEntity<Void> updateStatus(@RequestParam String uuid, @RequestParam Status status) {
        orderService.updateStatus(uuid, status);
        return ResponseEntity.ok().build();
    }

}
